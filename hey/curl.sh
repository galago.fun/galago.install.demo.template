#!/bin/bash

#url_api="http://localhost:8080"
url_api=$(gp url 8080)
function_name="tinygo.hey"
function_version="0.0.0"
data='{"name":"bob"}'
curl -d "${data}" \
  -H "Content-Type: application/json" \
  -X POST "${url_api}/functions/call/${function_name}/${function_version}"
echo ""
